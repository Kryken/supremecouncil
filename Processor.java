package com.hillel;

import com.hillel.executiveBranch.ElectionCommittee;
import com.hillel.executiveBranch.MinistryCabinet;
import com.hillel.legislature.Parliament;
import com.hillel.legislature.SupremeCouncil;

public class Processor {
    public static void main(String[] args) {
        SupremeCouncil supremeCouncil = new SupremeCouncil(null, null);
        Parliament parliament = supremeCouncil.assignMinister();
        parliament.work();
        MinistryCabinet ministryCabinet = new MinistryCabinet(null, supremeCouncil.passTheLowToTheMinistryCabinet());
        ministryCabinet.executeLaw();
        ElectionCommittee electionCommittee = ministryCabinet.passLaw();
        electionCommittee.work();
    }
}
