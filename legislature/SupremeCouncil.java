package com.hillel.legislature;

import com.hillel.Workable;

public class SupremeCouncil implements Workable {
    private Parliament parliament;
    private Law[] listOfLaw;

    public SupremeCouncil(Parliament parliament, Law[] listOfLaw ) {
        this.parliament = parliament;
        this.listOfLaw = listOfLaw;
    }

    public Parliament assignMinister() {
        return new Parliament(null);
    }

    public Law passTheLowToTheMinistryCabinet() {
        return new Law(null);
    }

    @Override
    public void work() {
        assignMinister();
        passTheLowToTheMinistryCabinet();
    }
}
