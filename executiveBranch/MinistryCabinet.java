package com.hillel.executiveBranch;

import com.hillel.Workable;
import com.hillel.legislature.Law;
import com.hillel.legislature.SupremeCouncil;

public class MinistryCabinet implements Workable {
    private Ministry[] listOfMinistry;
    private Law listOfLaw;


    public MinistryCabinet(Ministry[] listOfMinistry, Law listOfLaw) {
        this.listOfMinistry = listOfMinistry;
        this.listOfLaw = listOfLaw;
    }

    public ElectionCommittee passLaw() {
        return new ElectionCommittee(null, null);
    }

    public Law executeLaw() {
        return new Law(null);
    }

    @Override
    public void work() {
        passLaw();
        executeLaw();
    }
}
