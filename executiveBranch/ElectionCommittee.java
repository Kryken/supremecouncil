package com.hillel.executiveBranch;

import com.hillel.Workable;
import com.hillel.candidates.PresidentialCandidate;

public class ElectionCommittee implements Workable {
    private PollingStations[] listOfPollingStations;
    private PresidentialCandidate[] listOfPresidentialCandidate;

    public ElectionCommittee(PollingStations[] listOfPollingStations, PresidentialCandidate[] listOfPresidentialCandidate) {
        this.listOfPollingStations = listOfPollingStations;
        this.listOfPresidentialCandidate = listOfPresidentialCandidate;
    }

    @Override
    public void work() {
        System.out.println("Election day.");
    }
}
