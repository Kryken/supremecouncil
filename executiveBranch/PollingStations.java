package com.hillel.executiveBranch;

import com.hillel.People.Citizen;
import com.hillel.Workable;

public class PollingStations implements Workable {
    private String title;
    private String destination;
    private Citizen[] listOfCitizen;

    public PollingStations(String title, String destination, Citizen[] listOfCitizen) {
        this.title = title;
        this.destination = destination;
        this.listOfCitizen = listOfCitizen;
    }


    public Citizen vote() {
        return new Citizen(null, 0);
    }
    @Override
    public void work() {
        vote();
    }
}
